import currency
import memory
import temp
import unit

def clear():
    print("\033c")

def main():
    run = True
    while run == True:
        clear()
        print("Welcome to ConvertTheThings!")
        print("What would you like to do?")
        print("1. Currency Conversion")
        print("2. Temperature Conversion")
        print("3. Unit Conversion")
        print("4. Memory Conversion")
        print("0. Exit")
        uin = input("Choice: ")
        
        try:
            uin = int(uin)
            
            if(uin == 1):
                clear()
                currency.test()
                run = False
            elif(uin == 2):
                clear()
                temp.test()
                run = False
            elif(uin == 3):
                clear()
                unit.test()
                run = False
            elif(uin == 4):
                clear()
                memory.test()
                run = False
            elif(uin == 0):
                run = False
            else:
                print("You must enter a valid option. Please try again.")
                uin = input("Press enter to continue...")
                run = True
        except ValueError:
            print("You must enter a number. Please try again.")
            uin = input("Press enter to continue...")
            run = True

if __name__ == "__main__":
    main()