# ConvertTheThings  
  
## Project information  
### What can ConvertTheThings do?  
* Convert different types of money  
* Convert different units of measurment  
* Convert different units of time (hours to seconds, hours to minutes, etc)  
* Convery different units of computer storage (mb to gb, kb to tb, etc)  
  
### Why use ConvertTheThings?
Don't, if you don't want to.  
To me, convertTheThings is a useful tool becuase it has many different conversion types  
available in a central location. Which means that you don't have to scoure the web  
looking for each different type of converter when the need for it arives, instead  
its all right there in one simple, easy to use spot.  
  
#### Enjoy The Application, and please share feedback!